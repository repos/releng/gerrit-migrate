#!/usr/bin/bash

set -x
set -euo pipefail

# The goal here is to semi-automate closing a list of repos on Gerrit.
# The steps are as follows:
#
# 1. Make a shallow clone of each repo, checked out at default branch.
# 2. Delete everything in the repo.
# 3. Add a README pointing to the new location on GitLab.
# 4. Commit the above.
# 5. Push to the default branch.
# 6. Delete other branches?
# 7. ...what else?

CLONE_CMD="git clone --depth 1"
GERRIT_URL="ssh://gerrit.wikimedia.org:29418/"

# Get the current full set of descriptions:
ssh -p 29418 gerrit.wikimedia.org gerrit ls-projects --format json --description > descriptions.json

function clone () {
  basename="$(basename "$1")"
  $CLONE_CMD "${GERRIT_URL}${1}" "$basename"
}

function zap () {
  basename="$(basename "$1")"
  # Note quotes here - we want the wildcard interpreted by git, not the shell:
  git -C "$basename" rm -r '*'
}

function readme () {
  gerrit_repo="$1"
  basename="$(basename "$gerrit_repo")"
  gitlab_repo="$2"
  
  cat <<README > "$basename/README"
$gerrit_repo has moved to GitLab!

The $gerrit_repo repo has been moved to https://gitlab.wikimedia.org/$gitlab_repo

To update your repository in place you can run the following commands:

  git remote set-url origin git@gitlab.wikimedia.org:${gitlab_repo}.git

or

  git remote set-url origin https://gitlab.wikimedia.org/${gitlab_repo}.git

Then:

  git fetch origin
  git reset --hard origin/master
README
}

function set_desc () {
  gerrit_repo="$1"
  old_desc=$(jq -r '.["'$gerrit_repo'"].description' ./descriptions.json)
  new_desc="[MOVED TO GITLAB] $old_desc"
  echo "$new_desc";
}

# Repos are expected on STDIN, one line per:
while read -r gerrit_repo gitlab_repo; do
  clone "$gerrit_repo"
  zap "$gerrit_repo"
  readme "$gerrit_repo" "$gitlab_repo"
  set_desc "$gerrit_repo"
  sleep 1
done

# What's the deal with these?
# phabricator/extensions/security/
# phabricator/tools
